Beck Hearing Aid Centre is proud to be the oldest independently operated hearing clinic in Southwestern Ontario, celebrating 50 years in 2019. We are a family owned and operated clinic, and pride ourselves on our unsurpassed, patient-centred care.

Address: 396 Queens Ave, #102, London, ON N6B 1X7, Canada

Phone: 519-438-0492

Website: https://beckhearingaids.com
